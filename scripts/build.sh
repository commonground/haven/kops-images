#!/usr/bin/env sh
set -e

VERSION="1.19.1"

GIT_REPOSITORY="https://github.com/kubernetes/kops.git"
GIT_TAG="v${VERSION}"
BUILD_DIR="build/kops-${VERSION}"

if [ ! -d "build/images" ]; then
  mkdir -p "build/images"
fi

if [ ! -d "${BUILD_DIR}" ]; then
  mkdir -p ${BUILD_DIR}
fi

if [ ! -d "${BUILD_DIR}/.git" ]; then
  git clone --branch "${GIT_TAG}" --depth 1 "${GIT_REPOSITORY}" "${BUILD_DIR}"
fi

cd "${BUILD_DIR}"

git reset --hard

patch --strip=1 \
  --forward --no-backup-if-mismatch \
  --reject-file=- \
  < "../../patches/001-protokube-openstack.patch"

bazel build --action_env=PROTOKUBE_TAG=${VERSION} --platforms=@io_bazel_rules_go//go/toolchain:linux_amd64 //protokube/cmd/protokube:image-bundle-amd64.tar.gz.sha256

if [ -z "${CI_JOB_TOKEN}" ];then
  echo "Skipping upload of assets"
  exit 0
fi

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --upload-file bazel-bin/protokube/cmd/protokube/image-bundle-amd64.tar.gz \
  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/protokube/${VERSION}/protokube-amd64.tar.gz"

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --upload-file bazel-bin/protokube/cmd/protokube/image-bundle-amd64.tar.gz.sha256 \
  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/protokube/${VERSION}/protokube-amd64.tar.gz.sha256"
